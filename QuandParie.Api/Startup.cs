using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using QuandParie.Core.Application;
using QuandParie.Core.FakeDependencies.Gateway;
using QuandParie.Core.FakeDependencies.Persistance;
using QuandParie.Core.FakeDependencies.Services;
using QuandParie.Core.Gateway;
using QuandParie.Core.Persistance;
using QuandParie.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuandParie.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<BackOfficeAdministration>();
            services.AddSingleton<BalanceApplication>();
            services.AddSingleton<RegistrationApplication>();
            services.AddSingleton<WageringApplication>();

            services.AddSingleton<IBankingSystem, FakeBankingSystem>();
            services.AddSingleton<IAddressProofer, FakeAddressProofer>();
            services.AddSingleton<IIdentityProofer, FakeIdentityProofer>();
            
            services.AddSingleton<ICustomerRepository, InMemoryCustomerRepository>();
            services.AddSingleton<IDocumentRepository, InMemoryDocumentRepository>();
            services.AddSingleton<IOddsRepository, InMemoryOddsRepository>();
            services.AddSingleton<IWagerRepository, InMemoryWagerRepository>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "QuandParie.Api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "QuandParie.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
