﻿using Microsoft.AspNetCore.Mvc;
using QuandParie.Api.Contracts;
using QuandParie.Core.Application;
using QuandParie.Core.ReadOnlyInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QuandParie.Api.Controllers
{
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly RegistrationApplication application;

        public RegistrationController(RegistrationApplication application)
        {
            this.application = application;
        }

        [HttpPost("/customers")]
        public async Task<IReadOnlyCustomer> CreateCustomer(CustomerCreation customer)
        {
            return await application.CreateAccount(
                email: customer.Email,
                firstName: customer.FirstName,
                lastName: customer.LastName);
        }

        [HttpGet("/customers/{email}")]
        public async Task<ActionResult<IReadOnlyCustomer>> GetCustomer(string email)
        {
            var result = await application.GetAccount(email);

            return result != null
                ? new ActionResult<IReadOnlyCustomer>(result)
                : NotFound();
        }

        [HttpPut("/customers/{email}/identityProof")]
        public async Task<ActionResult> UploadIdentityProof(string email)
        {
            if (await application.GetAccount(email) == null)
                return NotFound();

            return await application.UploadIdentityProof(email, await ReadBodyAsByteAsync())
                ? Ok("The document is valid")
                : BadRequest("The document is invalid");
        }

        [HttpPut("/customers/{email}/addressProof")]
        public async Task<ActionResult> UploadAddressProof(string email)
        {
            if (await application.GetAccount(email) == null)
                return NotFound();

            return await application.UploadAddressProof(email, await ReadBodyAsByteAsync())
                ? Ok("The document is valid")
                : BadRequest("The document is invalid");
        }

        private async Task<byte[]> ReadBodyAsByteAsync()
        {
            using var memoryStream = new MemoryStream(2048);
            await Request.Body.CopyToAsync(memoryStream);
            return memoryStream.ToArray();
        }
    }
}
