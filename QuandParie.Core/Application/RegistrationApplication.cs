﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {


        private readonly ICustomerRepository Customer;


        private readonly IDocumentRepository Document;

  
        private readonly IIdentityProofer IdentityProofer;

 
        private readonly IAddressProofer AddressProofer;

        public RegistrationApplication(
            ICustomerRepository Customer, 
            IDocumentRepository Document, 
            IIdentityProofer IdentityProofer,
            IAddressProofer AddressProofer)
        {
            this.Customer = Customer;
            this.Document = Document;
            this.IdentityProofer = IdentityProofer;
            this.AddressProofer = AddressProofer;
        }


        public async Task<IReadOnlyCustomer> CreateAccount(string email, string firstName, string lastName)
        {
            var customer = new Customer(email, firstName, lastName);
            await Customer.SaveAsync(customer);

            return customer;
        }


        public async Task<IReadOnlyCustomer> GetAccount(string email)
        {
            return await Customer.GetAsync(email);
        }


        public async Task<bool> UploadIdentityProof(string docId, byte[] identityProof)
        {
            var customer = await Customer.GetAsync(docId);
            if (!IdentityProofer.Validates(customer, identityProof))
                return false;

            await Document.SaveAsync(DocumentType.IdentityProof, docId, identityProof);

            customer.IsIdentityVerified = true;
            await Customer.SaveAsync(customer);

            return true;
        }

      
        public async Task<bool> UploadAddressProof(string docId, byte[] addressProof)
        {
            var customer = await this.Customer.GetAsync(docId);
            if (!AddressProofer.Validates(customer, out var address, addressProof))
                return false;

            await Document.SaveAsync(DocumentType.AddressProof, docId, addressProof);

            customer.Address = address;
            await this.Customer.SaveAsync(customer);

            return true;
        }
    }
}
